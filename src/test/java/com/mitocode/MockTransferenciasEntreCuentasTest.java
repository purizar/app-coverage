package com.mitocode;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class MockTransferenciasEntreCuentasTest {
	
	private final static Logger log = Logger.getLogger(MockTransferenciasEntreCuentasTest.class);
	
	@Mock
	private Cuenta cuentaOrigen;
	
	@Mock
	private Cuenta cuentaDestino;
	
	@Test
	public void transferenciaEntreCuentas() throws CuentaException {

		log.info("Test: Transferencia entre Cuentas OK");
		log.info("Monto a transferir: " + 4000.00);
		
		
		doReturn(5000.00).when(cuentaOrigen).getMonto();
		doReturn(1000.00).when(cuentaDestino).getMonto();

		assertEquals(cuentaOrigen.getMonto(), 5000.00, 0);
		assertEquals(cuentaDestino.getMonto(), 1000.00, 0);
		
		when(cuentaOrigen.tranferencia(cuentaDestino, 4000.00)).thenReturn(true);

		assertEquals(cuentaOrigen.tranferencia(cuentaDestino, 4000.00), true);
	}
	
	public void printAccounts() {
		log.info(cuentaOrigen + " " + cuentaDestino);
	}

}
